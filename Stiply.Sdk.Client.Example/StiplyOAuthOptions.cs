﻿using System;

namespace Stiply.Sdk.Client.Example
{
    public class StiplyOAuthOptions
    {
        public Uri OAuthEndpointUrl { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public int LocalPort { get; set; }
    }
}
