﻿using RichardSzalay.MockHttp;
using System.IO;
using System.Net;

namespace RichardSzalay.MockHttp
{
    public static class MockedRequestExtensions
    {
        public static MockedRequest RespondWithJsonFile(this MockedRequest request, string filename)
        {
            var absolutePath = Path.GetFullPath(filename);
            return request.Respond(HttpStatusCode.OK, "application/json", File.ReadAllText(absolutePath));
        }
    }
}
