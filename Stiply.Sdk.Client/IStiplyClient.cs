﻿using System.Threading.Tasks;

namespace Stiply.Sdk
{
    /// <summary>
    /// Interface for the Stiply Client
    /// </summary>
    public interface IStiplyClient
    {

        /// <summary>
        /// Test the API connections and authentication header
        /// </summary>
        /// <param name="ping"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<PingResponse> Ping(string ping, string token);

        /// <summary>
        /// Create new signature request.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<CreatedSignRequest> CreateSignRequest(CreateSignRequest model, string token);

        /// <summary>
        /// Add signer to sign request. This url may be called multiple times on one sign request to add multiple signers to the sign request.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<CreatedSigner> CreateSigner(CreateSigner model, string signRequestKey, string token);

        /// <summary>
        /// Send sign request to first signer
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<SentSignRequest> SendSignRequest(string signRequestKey, string token);

        /// <summary>
        /// Get signed document of sign request.
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<DocumentResponse> GetSignedDocument(string signRequestKey, string token);

        /// <summary>
        /// Get proof document of sign request.
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<DocumentResponse> GetProofDocument(string signRequestKey, string token);

        /// <summary>
        /// Send a reminder e-mail to the current signer of the sign request.
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<SendReminderResponse> SendReminder(string signRequestKey, string token);

        /// <summary>
        /// Extend the term of an existing sign request.
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="term"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<ExtendedTerm> ExtendTerm(string signRequestKey, string term, string token);

        /// <summary>
        /// Get sign request key from external key (your local key).
        /// </summary>
        /// <param name="externalKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<SignRequestKey> GetSignRequestByExternalKey(string externalKey, string token);

        /// <summary>
        /// Get a specific sign request
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<CreatedSignRequest> GetSignRequest(string signRequestKey, string token);

        /// <summary>
        /// Delete a specific sign request
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<DeleteResponse> DeleteSignRequest(string signRequestKey, string token);

        /// <summary>
        /// Get a specific signer
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="signerKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<SignerResponse> GetSigner(string signRequestKey, string signerKey, string token);

        /// <summary>
        /// Update the e-mail address and/or telephone number of a signer who has not yet signed. It is required to post both values, email and phone, when updating. Should you only want to update one of these, just submit the original value of the other.
        /// Please note that in case a signer has already received the sign request email and you update the email address of the signer afterwards, the link in the signer’s e-mail will not work anymore.The signer shall not receive a new sign request e-mail on the new e-mail address.Please use Send Reminder for that purpose after the e-mail address has been updated.If you only update the phone number the original sign link still works so no reminder is required.
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="signerKey"></param>
        /// <param name="newEmail"></param>
        /// <param name="newPhoneNumber"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<UpdatedSigner> UpdateSigner(string signRequestKey, string signerKey, string newEmail, string newPhoneNumber, string token);

        /// <summary>
        /// Delete a specific signer
        /// </summary>
        /// <param name="signRequestKey"></param>
        /// <param name="signerKey"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<DeleteResponse> DeleteSigner(string signRequestKey, string signerKey, string token);

    }
}
