using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The properties of the update signer request.
    /// </summary>
    public partial class UpdateSignerRequest
    {
        /// <summary>
        /// New email of the signer to which the email will be sent. This value will replace the e-mail of the signer permanently
        /// </summary>
        [JsonProperty("new_email", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string NewEmail { get; set; }

        /// <summary>
        /// New phone number of signer to which the authentication text will be send. This value will replace phone number of the signer permanently
        /// </summary>
        [JsonProperty("new_cell_phone_number", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string NewCellPhoneNumber { get; set; }
    }
}