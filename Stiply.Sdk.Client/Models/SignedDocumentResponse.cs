using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The reponse of a signed document or proof document.
    /// </summary>
    public partial class DocumentResponse
    {
        /// <summary>
        /// Get or sets the filename.
        /// </summary>
        [JsonProperty("file_name")]
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the mime type
        /// </summary>
        [JsonProperty("mimeType")]
        public string MimeType { get; set; }

        /// <summary>
        /// Gets or sets the file size.
        /// </summary>
        [JsonProperty("file_size")]
        public long? FileSize { get; set; }

        /// <summary>
        /// Gets or set sthe file's hash
        /// </summary>
        [JsonProperty("hash")]
        public string Hash { get; set; }

        /// <summary>
        /// Gets or sets the document's content
        /// </summary>
        [JsonProperty("data")]
        public byte[] Data { get; set; }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        [JsonProperty("status_code")]
        public long? StatusCode { get; set; }
    }
}