using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The response of a sent sign request.
    /// </summary>
    public class SentSignRequest
    {
        /// <summary>
        /// The data response object.
        /// </summary>
        [JsonProperty("data")]
        public Data Data { get; set; }
    }
}