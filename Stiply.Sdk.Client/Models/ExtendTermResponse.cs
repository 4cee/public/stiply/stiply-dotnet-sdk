using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The response of an extended term.
    /// </summary>
    public partial class ExtendedTerm
    {
        /// <summary>
        /// Gets or sets the signer data.
        /// </summary>
        [JsonProperty("data")]
        public Data Data { get; set; }
    }
}