
using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The ping request
    /// </summary>
    public class PingRequest
    {
        /// <summary>
        /// A string to send to the API. If the API responds correctly the same string will be returned
        /// </summary>
        [JsonProperty("ping_value", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string ping_value { get; set; }
    }
}