using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The response of a ping.
    /// </summary>
    public class PingResponse
    {
        /// <summary>
        /// Gets or sets the ping value.
        /// </summary>
        [JsonProperty("ping_value")]
        public string PingValue { get; set; }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        [JsonProperty("status_code")]
        public long? StatusCode { get; set; }

    }
}