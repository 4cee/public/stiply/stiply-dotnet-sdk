﻿namespace Stiply.Sdk
{
    /// <summary>
    /// The request of a create sign request.
    /// </summary>
    public class CreateSignRequest
    {
        /// <summary>
        /// Filename of the file that will be uploaded
        /// </summary>
        public string fileName { get; set; }

        /// <summary>
        /// The file to be uploaded in DOC, DOCX or PDF
        /// </summary>
        public byte[] file { get; set; }
        /// <summary>
        /// 2 digit code representing the sign term
        /// </summary>
        /// <value>1d = 1 day, 2w = 2 weeks, 3m = 3 months</value>

        public string term { get; set; }


#nullable enable
        /// <summary>
        /// The name of the document. Please note this is not the name of the file but merely a label for the document.
        /// </summary>

        public string? docName { get; set; }
        /// <summary>
        /// The message to be included in the e-mail to the signers.
        /// </summary>
        /// <value></value>

        public string? message { get; set; }
        /// <summary>
        /// A comment for internal use.
        /// </summary>

        public string? comment { get; set; }
        /// <summary>
        /// A key for your internal use so you don’t have to save the Stiply sign request key in your local database. However, your external key has to be unique.
        /// </summary>

        public string? externalKey { get; set; }
        /// <summary>
        /// An URL to be called by Stiply when the last signer has signed the document. Please note that ?key={sign_request_key} shall be added to the call back url.
        /// </summary>

        public string? callBackUrl { get; set; }
    }
}
