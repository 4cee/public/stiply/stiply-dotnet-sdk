using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The response of a created signer
    /// </summary>
    public class CreatedSigner
    {
        /// <summary>
        /// The Data response object
        /// </summary>
        [JsonProperty("data")]
        public SignerData Data { get; set; }
    }

}