using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The response of a created SignRequest
    /// </summary>
    public partial class CreatedSignRequest
    {
        /// <summary>
        /// The Data response object
        /// </summary>
        [JsonProperty("data")]
        public Data Data { get; set; }
    }
}