using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// DTO of the document in a sign request
    /// </summary>
    public partial class Document
    {
        /// <summary>
        /// Gets the name of the document.
        /// </summary>
        [JsonProperty("name")]
        public dynamic Name { get; set; }

        /// <summary>
        /// Gets the filename of the document
        /// </summary>
        [JsonProperty("filename")]
        public string Filename { get; set; }

        /// <summary>
        /// Gets the file size of the document
        /// </summary>
        [JsonProperty("file_size")]
        public long? FileSize { get; set; }

        /// <summary>
        /// Gets the moment when the document is created
        /// </summary>
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        /// <summary>
        /// Gets the moment when the document is last updated.
        /// </summary>
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
    }
}