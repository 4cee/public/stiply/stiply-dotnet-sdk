using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// The response of GetSignRequestByExtenalKey.
    /// </summary>
    public partial class SignRequestKey
    {
        /// <summary>
        /// Gets or sets the sign request key.
        /// </summary>
        [JsonProperty("key")]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        [JsonProperty("status_code")]
        public long? StatusCode { get; set; }
    }
}