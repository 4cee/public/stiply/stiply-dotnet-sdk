﻿using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// Idin authentication properties
    /// </summary>
    public class PimId
    {
        /// <summary>
        /// The prefix of the name of the signer, for example if the signers name is Mike van der Meer, van der would be the prefix.
        /// </summary>
        [JsonProperty("initials", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string Initials { get; set; }

        /// <summary>
        /// The lastname of the signer, in the above example this would be Meer.
        /// </summary>
        [JsonProperty("family_name", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string FamilyName { get; set; }

        /// <summary>
        /// provide the date of birth of the signer here in the following format: dd-mm-yyyy, so for example 01-09-1982
        /// </summary>
        [JsonProperty("date_of_birth", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
        public string DateOfBirth { get; set; }
    }
}
