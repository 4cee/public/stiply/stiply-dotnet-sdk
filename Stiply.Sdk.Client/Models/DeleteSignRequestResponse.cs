using Newtonsoft.Json;

namespace Stiply.Sdk
{
    /// <summary>
    /// Response of deleted sign request.
    /// </summary>
    public partial class DeleteResponse
    {
        /// <summary>
        /// The message of the response.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// The status code of the response.
        /// </summary>
        [JsonProperty("status_code")]
        public long? StatusCode { get; set; }
    }
}