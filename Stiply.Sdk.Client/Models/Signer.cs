using Newtonsoft.Json;
using System.Collections.Generic;

namespace Stiply.Sdk
{
    /// <summary>
    /// A signer that must sign a sign request.
    /// </summary>
    public partial class Signer
    {
        /// <summary>
        /// Gets or sets the signer's key
        /// </summary>
        [JsonProperty("key")]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the e-mail address of signer
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the signer's name.
        /// </summary>
        [JsonProperty("name")]
        public dynamic Name { get; set; }

        /// <summary>
        /// Gets or sets the signer's order.
        /// </summary>
        [JsonProperty("order")]
        public dynamic Order { get; set; }

        /// <summary>
        /// Gets or sets the signer's phone number
        /// </summary>
        [JsonProperty("phone")]
        public dynamic Phone { get; set; }

        /// <summary>
        /// Gets or sets the signer's authentication method.
        /// </summary>
        [JsonProperty("auth_method")]
        public dynamic AuthMethod { get; set; }

        /// <summary>
        /// Gets or sets the moment the signer is created.
        /// </summary>
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets the moment the signer is last updated.
        /// </summary>
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        /// <summary>
        /// Gets or sets the URL the signer can sign the document.
        /// </summary>
        [JsonProperty("sign_url")]
        public dynamic SignUrl { get; set; }

        /// <summary>
        /// Gets or sets the progress of the signer.
        /// </summary>
        [JsonProperty("signer_progresses")]
        public List<SignerProgress> SignerProgresses { get; set; }
    }
}