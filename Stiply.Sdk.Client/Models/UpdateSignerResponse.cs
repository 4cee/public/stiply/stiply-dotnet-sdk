using Newtonsoft.Json;
using System.Collections.Generic;

namespace Stiply.Sdk
{
    /// <summary>
    /// The updated signer response.
    /// </summary>
    public partial class UpdatedSigner
    {
        /// <summary>
        /// The Data response object.
        /// </summary>
        [JsonProperty("data")]
        public SignerData Data { get; set; }
    }

}