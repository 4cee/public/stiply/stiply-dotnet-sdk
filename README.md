# Stiply .NET SDK
[![Nuget downloads][downloads-image]][downloads-url]

An official library for using the Stiply API written in C#.NET and powered by Refit.


## Table of Contents
  - [Installation](#installation)
  - [Usage](#usage)
    - [Authentication](#authentication)
    - [Available Methods](#available-methods)
  - [Example](#example)


## Installation
The Stiply .NET SDK can be installed using the NuGet package manager, under the package name Stiply (package details).

## Usage
First, use our namespace:

```csharp
using Stiply;
```

Create a client object:

```csharp
var client = new HttpClient()
 {
    BaseAddress = new Uri("https://api.stiply.nl"),
 };

var stiplyClient = new StiplyClient(client);
```

## Authentication

At Stiply we support a variety of authentication methods ([see our documentation][api-docs]). In the end, all of these methods will provide an access token that needs to be used for the API calls.
Because of this reason we have to add a token property for each of the API functions.

```csharp
var bearerToken = "Bearer TOKEN";
 var response = stiplyClient.Ping("ping", token);
```

The Stiply SDK will add the token on to the `Authorization` Header.

## Available Methods
All current API endpoints are represented by the following corresponding methods in the SDK, documentation around this can be ([in our documentation][api-docs])



## Example

The following example demonstrates how to create a new sign request, add a signer, and finally send the sign request using the SDK.

The first step will be to create a new instance of the `StiplyClient` class, which we can then use to communicate with the _Stiply_ API:

```csharp
var client = new HttpClient()
 {
    BaseAddress = new Uri("https://api.stiply.nl"),
 };

var stiplyClient = new StiplyClient(client);
var token = "Bearer: your token"
```

Next, we will create a new sign request:

```csharp
string documentPath = "document.pdf";
byte[] fileBytes = File.ReadAllBytes(path);
var signRequest = new CreateSignRequest()
{
    term = "1w",
    fileName = "filename.pdf",
    file = fileBytes
};
var signRequestResponse = await _stiplyClient.CreateSignRequest(signRequest, token);
var srKey = signRequestResponse.Data.SignRequest.Key;
```

We can now add a signer to the newly created sign request:

```csharp
// Previously discussed code is here

var signature1 = new SignerSignatureField() { Name = "signature_0" };
var signer = new CreateSigner()
{
    signerEmail = "example@stiply.nl",
    signerSignatureFields = new List<SignerSignatureField>() { signature1 }
};
// srKey is the sign request key from the response of our previous example
var signerResponse = await _stiplyClient.CreateSigner(signer, srKey, token);
```

> _**Important**_ It is assumed that our previously uploaded document `document.pdf` contains one or more tags that are formatted as `{{signature_0}}`.

Finally, we can send the sign request to the first (and in this case only) signer:

```csharp
var response = await _stiplyClient.SendSignRequest(srKey, token);
```

This signer should now receive a mail with a link to our viewer application where one can sign the uploaded document digitally.

**Summary**: 
The complete code for creating a new sign request, adding a single signer and sending the sign request will look something like:

```csharp
var client = new HttpClient()
 {
    BaseAddress = new Uri("https://api.stiply.nl"),
 };

var stiplyClient = new StiplyClient(client);
var token = "Bearer: your token"

string documentPath = "document.pdf";
byte[] fileBytes = File.ReadAllBytes(path);
var signRequest = new CreateSignRequest()
{
    term = "1w",
    fileName = "filename.pdf",
    file = fileBytes
};
var signRequestResponse = await _stiplyClient.CreateSignRequest(signRequest, token);
var key = signRequestResponse.Data.SignRequest.Key;

var signature1 = new SignerSignatureField() { Name = "signature_0" };
var signer = new CreateSigner()
{
    signerEmail = "example@stiply.nl",
    signerSignatureFields = new List<SignerSignatureField>() { signature1 }
};

await _stiplyClient.CreateSigner(signer, key, token);

var response = await _stiplyClient.SendSignRequest(srKey, token);

```
[nuget-url]: https://www.nuget.org/packages/stiply-sdk.dll
[downloads-image]: https://img.shields.io/nuget/dt/tiply-sdk.dll.svg?style=flat.
[api-docs]: https://www.stiply.nl/api
